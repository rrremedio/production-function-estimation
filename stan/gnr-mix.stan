data {
  int<lower=1> NT;
  int<lower=1> NTmiss;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  vector[NT] m;
  real k[NT];
  real l[NT];
  vector[NT] s;
  real y[NT];
  vector[nmix] priorPi;
  int iMiss[NTmiss];
  int tMiss[NTmiss];
  int type[NT];
  #real w[N,nmix];
}


parameters {
  real rho[nmix];
  simplex[nmix] prob;
  real<lower=0.0, upper=1.0> bm[nmix];
  #positive_ordered[nmix] bm;
  real<lower=0.0, upper=2.0>  bk[nmix];
  real<lower=0.0, upper=2.0>  bl[nmix];
  real<lower=0> sigeps[nmix];
  real<lower=0> sigeta[nmix];
  #real<lower=0> sigom0[nmix];
  real b0[T,nmix];

  real rhoX[2,4,nmix]; # coefficient for k,l process
  cov_matrix[2] varX[nmix]; # var for k,l process
  cov_matrix[3] var0[nmix]; # initial variance of omega,l,k
  vector[3] mu0[nmix];
}

transformed parameters {
  real logE[nmix];
  vector[NT] omega[nmix];
  for(j in 1:nmix) {
    logE[j] <- 0.5*sigeps[j]^2;
    for(n in 1:NT) {
      omega[j][n] <- m[n] - (log(bm[j]) + logE[j] + b0[time[n],j] +
                             bk[j]*k[n] + bl[j]*l[n] +
                             bm[j]*(log(bm[j]) + logE[j]))/(1-bm[j]);
    }
  }
}

model {
  vector[nmix] p;
  #bk ~ normal(0.35,2); 
  #bl ~ normal(0.35,2); 
  #rho ~ normal(0,10);
  #b0 ~ normal(0,10);
  #sigeta ~ gamma(2,0.1);

  sigeta ~ cauchy(0,5);
  sigeps ~ cauchy(0,5);
  prob ~ dirichlet(priorPi);
  
  for (n in 1:NT) {
    if (n==1 || id[n]!=id[n-1]) { # new observation
      vector[3] z0;
      z0[2] <- l[n];
      z0[3] <- k[n];
      for (j in 1:nmix) {
        z0[1] <- omega[j][n];
        p[j] <- log(prob[j]) + multi_normal_log(z0,mu0[j],var0[j]) - log(1-bm[j]);
      }
    } else {
      vector[2] zt;
      zt[1] <- l[n];
      zt[2] <- k[n];
      for (j in 1:nmix) {
        vector[2] mut;
        for(a in 1:2) {
          mut[a] <- rhoX[a,1,j] + rhoX[a,2,j]*omega[j][n-1] +
            rhoX[a,3,j]*l[n-1] + rhoX[a,4,j]*k[n-1];
        }
        p[j] <- p[j] +
          normal_log(omega[j][n],rho[j]*omega[j][n-1],sigeta[j]/(1-bm[j]))
          +
          multi_normal_log(zt,mut,varX[j]);
      }
    }
    for(j in 1:nmix) {
      p[j] <- p[j] + normal_log(s[n],log(bm[j])+logE[j],sigeps[j]);
    }
    if (n==NT || id[n+1]!=id[n]) {
      increment_log_prob(log_sum_exp(p));
      #increment_log_prob(p[type[n]]);
    }
  }
}

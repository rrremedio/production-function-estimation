data {
  int<lower=1> NT;
  int<lower=1> nmix;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real s[NT];
  real y[NT];
}
parameters {
  #real rho;
  #real b0[nmix];
  real<lower=0> bm[nmix];
  simplex[nmix] pi;
  #real bk;
  #real bl;
  real<lower=0> sigeps[nmix];
  #real<lower=0> Vom;
}

model {
  real logE[nmix];
  real ps[nmix];

  pi ~ dirichlet(priorPi);
  for (j in 1:nmix) {
    logE[j] <- (sigeps[j]^2/2);
  }
  for(n in 1:NT) {
    if (n>1 && id[n-1]==id[n]) { # && time[n-1]==(time[n]-1)) {
      for(j in 1:nmix) {
        ps[j] <- ps[j] + normal_log(-s[n], -log(bm[j])-logE[j],sigeps[j]);
      }
    } else {
      if (n>1) {
        #print("id ",id[n]," like ", log_sum_exp(ps), "ps ",ps);
        increment_log_prob(log_sum_exp(ps));
      }
      # starting a new person
      for (j in 1:nmix) {    
        ps[j] <- log(pi[j]);
      }
    }
    #if (id[n]<200) {
    #  print(" n ", n, " t ", time[n], " ps ", ps, " pi ", pi);
    #}
  }
}

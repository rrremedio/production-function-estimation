    if (0 && n>1 && id[n-1]==id[n]) {  
      if (time[n-1]==(time[n]-1)) {
        vector[3] inputs;
        inputs[1] <- m[n];
        inputs[2] <- k[n];
        inputs[3] <- l[n];
        for(j in 1:nmix) {
          vector[3] mu;
          for (i in 1:3) {
            mu[i] <- rhox[j][i,1] + rhox[j][i,2]*omega[j,time[n-1]] +
                     rhox[j][i,3]*m[n-1] + rhox[j][i,4]*k[n-1] +
                     rhox[j][i,5]*l[n-1];
          }
          #print(" mu ",mu, omega[j,time[n-1]], m[n-1], k[n-1], l[n-1]);
          ps[j] <- ps[j] + multi_normal_log(inputs, mu, Vin[j]);
          ps[j] <- ps[j] + normal_log(omega[j,time[n]], rho*omega[j,time[n-1]], sigeta);
        }
        #print(" after stage 2: ps=",ps); 
      } else if (0 && newObs) {
        vector[4] inputs;
        inputs[1] <- m[n];
        inputs[2] <- k[n];
        inputs[3] <- l[n];
        for(j in 1:nmix) {
          inputs[4] <- omega[j,time[n]];
          ps[j] <- ps[j] + multi_normal_log(inputs, mean0[j], V0[j]);
        }
        #print(" after time 1: ps=",ps);
      }

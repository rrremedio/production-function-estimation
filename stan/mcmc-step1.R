rm(list=ls())
## load data
df <- read.csv("data_production_function_missing2zero_ver2.csv")
df[df==0] <- NA
df$t <- df$year
df <- df[order(df$id,df$t),]
ind12 <- subset(df,industry_2==12)

## only use first observed spell
source("productionEstimation.R")
library(data.table)
ind12 <- ind12[order(ind12$id,ind12$t),]
id.lag <- panel.lag(ind12$id,ind12$id,ind12$t)
t.lag <- panel.lag(ind12$t,ind12$id,ind12$t)
y.lag <- panel.lag(ind12$y_it,ind12$id,ind12$t)
new.spell <- ((ind12$id==id.lag & ind12$t!=(t.lag+1)) |
              (ind12$id!=id.lag) |
              (!is.na(ind12$y_it) & is.na(y.lag)))
dt <- data.table(cbind(ind12,new.spell))
dt[,spell.num := cumsum(new.spell), by=id]
dt <- dt[dt$spell.num==1,]

## drop firms observed < 4 times
dt[,n.obs := sum(!is.na(y_it)), by=id]
dt <- dt[dt$n.obs>=4,]
ind12 <- data.frame(dt)[,1:13]

ind12 <- ind12[order(ind12$id,ind12$t),]

library(rstan)
model.stan <- "cd-mix.stan"
# Translate STAN code to C++, complie C++, load library into R
stan.mix.step1 <- stan_model(file=model.stan,
                             model_name="Cobb-Douglas, first stage",
                             verbose=TRUE)

nmix <- 1

initial.values <- list(bm=rep(0.37,nmix), #b0=rep(0,nmix),
                       pi=rep(1/nmix,nmix),
                       sigeps=rep(sd(ind12$lnmY_it),nmix)
                       )

stan.data <- list(nmix=nmix, NT=nrow(ind12), id=ind12$id, time=ind12$t-min(ind12$t)+1,
                  m=ind12$m_it, k=ind12$k_it, l=ind12$l_it, s=ind12$lnmY_it,
                  y=ind12$y_it, T=max(ind12$t)-min(ind12$t)+1,
                  priorPi=as.vector(rep(2,nmix)))
if (nmix==1) {
  dim(stan.data$priorPi) <- 1
}

fit <- sampling(stan.mix.step1, data=stan.data, #init=list(initial.values),
                iter=1000, chains=1, algorithm="NUTS",warmup=100,verbose=TRUE)

mle <- optimizing(stan.mix.step1, data=stan.data,
                  #init=list(initial.values),
                  verbose=TRUE)



rm(list=ls())
df <- read.csv("data_production_function_missing2zero_ver2.csv")
df[df==0] <- NA
outDir <- "./figure"
fontFamily <- "Bookman"
fdims <- 1.5*c(5.03937, 3.77953) # 1.5 times dimensions of beamer slide
printDev <- function(filename, scale=1, width=scale*fdims[1], height=scale*fdims[2], family=fontFamily) {
  cairo_pdf(filename,width=width,height=height,family=family)
  #pdf(filename,width,height)
}

source("productionEstimation.R")
df$t <- df$year
df <- df[order(df$id,df$t),]
ind12 <- subset(df,industry_2==12)
#ind12 <- subset(df,industry ==2914)
#<=2915 & industry>=2911)

## Save some tables graphs for putting into tex file
library(stargazer)
filename <- paste(outDir,"sumstats.tex",sep="/")
stargazer(ind12[,c("y_it","lnmY_it","m_it","k_it","l_it","export","I_K","I_K_m","industry")],title="Summary statistics", label="tab:sum", header=FALSE, out=filename)


library(ggplot2)
library(RColorBrewer)
colors <- c(brewer.pal(12,"Set3")) #,brewer.pal(9,"Pastel1"))
idcolors <-
  rep(colors,times=ceiling(length(unique(ind12$id))/length(colors)))


# line plots against year across different industries
ind.codes <- unique(ind12$industry)
for (i in ind.codes) {
  filename <- sprintf("%s/trend-share-%d.pdf",outDir,i)
  fig <- ggplot(data=subset(ind12,industry==i),aes(x=year, y=exp(lnmY_it), colour=as.factor(id), group=id)) +
    geom_line(alpha=1, size=0.1) + scale_colour_manual(values=idcolors) +
      theme_minimal() + theme(legend.position="none") + ylim(c(0,1))
  printDev(filename, height=fdims[2]*2/3)
  print(fig) # need to explicitly print the fig for it to display in
             # a loop
  dev.off()
}

# Scatter plots
printDev(paste(outDir,"scatter-yk.pdf",sep="/"),scale=1/2)
ggplot(data=ind12,aes(x=k_it, y=y_it, colour=as.factor(id), group=id)) +
  geom_point() + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()
printDev(paste(outDir,"scatter-yl.pdf",sep="/"),scale=1/2)
ggplot(data=ind12,aes(x=l_it, y=y_it, colour=as.factor(id), group=id)) +
  geom_point() + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()
printDev(paste(outDir,"scatter-ym.pdf",sep="/"),scale=1/2)
ggplot(data=ind12,aes(x=m_it, y=y_it, colour=as.factor(id), group=id)) +
  geom_point() + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()

printDev(paste(outDir,"scatter-sk.pdf",sep="/"),scale=1/2)
ggplot(data=ind12,aes(x=k_it, y=lnmY_it, colour=as.factor(id), group=id)) +
  geom_point() + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none") +
  annotate("text",x=median(ind12$k_it)+0.5,y=min(ind12$lnmY_it)+0.5,
           label=sprintf("Corr(lnmY,k) = %.3f",cor(ind12$k_it,ind12$lnmY_it,use="complete.obs")))
dev.off()

printDev(paste(outDir,"scatter-sl.pdf",sep="/"),scale=1/2)
ggplot(data=ind12,aes(x=l_it, y=lnmY_it, colour=as.factor(id), group=id)) +
  geom_point() + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none") +
  annotate("text",x=median(ind12$l_it),y=min(ind12$lnmY_it)+0.5,
           label=sprintf("Corr(lnmY,l) = %.3f",cor(ind12$l_it,ind12$lnmY_it,use="complete.obs")))
dev.off()

printDev(paste(outDir,"scatter-sm.pdf",sep="/"),scale=1/2)
ggplot(data=ind12,aes(x=m_it, y=lnmY_it, colour=as.factor(id), group=id)) +
  geom_point() + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none") +
  annotate("text",x=median(ind12$m_it),y=min(ind12$lnmY_it)+0.5,
           label=sprintf("Corr(lnmY,m) = %.3f",cor(ind12$m_it,ind12$lnmY_it,use="complete.obs")))
dev.off()


# line plots against year
printDev(paste(outDir,"trend-share.pdf",sep="/"),height=fdims[2]*2/3)
ggplot(data=ind12,aes(x=year, y=exp(lnmY_it), colour=as.factor(id), group=id)) +
  geom_line(alpha=1, size=0.1) + scale_colour_manual(values=idcolors)+
  theme_minimal() + theme(legend.position="none") + ylim(c(0,1))
dev.off()
printDev(paste(outDir,"trend-s.pdf",sep="/"),height=fdims[2]*2/3)
ggplot(data=ind12,aes(x=year, y=lnmY_it, colour=as.factor(id), group=id)) +
  geom_line(alpha=1, size=0.1) + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()
printDev(paste(outDir,"trend-y.pdf",sep="/"),height=fdims[2]*2/3)
ggplot(data=ind12,aes(x=year, y=y_it, colour=as.factor(id), group=id)) +
  geom_line(size=0.1) + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()
printDev(paste(outDir,"trend-k.pdf",sep="/"),height=fdims[2]*2/3)
ggplot(data=ind12,aes(x=year, y=k_it, colour=as.factor(id), group=id)) +
  geom_line(size=0.1) + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()
printDev(paste(outDir,"trend-l.pdf",sep="/"),height=fdims[2]*2/3)
ggplot(data=ind12,aes(x=year, y=l_it, colour=as.factor(id), group=id)) +
  geom_line(size=0.1) + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()
printDev(paste(outDir,"trend-m.pdf",sep="/"),,height=fdims[2]*2/3)
ggplot(data=ind12,aes(x=year, y=m_it, colour=as.factor(id), group=id)) +
  geom_line(size=0.1) + scale_colour_manual(values=idcolors) +
  theme_minimal() + theme(legend.position="none")
dev.off()

